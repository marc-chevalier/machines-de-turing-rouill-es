#include "../include/m_trans.h"

MTrans::MTrans(const string m_fonc_e, const string symbole_e, const string operation_e, const string m_config_resultante_e)
{
    m_fonc=m_fonc_e;
    symbole=symbole_e;
    operation=operation_e;
    m_config_resultante=m_config_resultante_e;

}

void MTrans::exec(vector<string>& rubanG, vector<string>& rubanD, int& pos, string& m_config, const string& BG, const string& BD, const string& m_fonction)
{
    vector<pair<string,string> > op=analyse_syntax_operation(operation,analyse_syntax_m_conf(m_config),analyse_syntax_m_conf(m_fonction));
    int position_prec=pos;
    long unsigned int n=op.size();

    for(unsigned int i=0;i<n;++i)
    {
        if(op[i].first=="E")
        {
            if(pos>=0)
                rubanD[static_cast<size_t>(pos)]=BD;
            else
                rubanG[static_cast<size_t>(-pos-1)]=BG;
        }
        else if(op[i].first=="D")
        {
            ++pos;
            if(pos>=0 && static_cast<int>(rubanD.size())<pos+5)
                rubanD.resize(static_cast<size_t>(pos+30),BD);
            else if(pos<0 && static_cast<int>(rubanG.size())<pos+5)
                rubanG.resize(static_cast<size_t>(-pos+30),BG);
        }
        else if(op[i].first=="G")
        {
            --pos;
            if(pos>=0 && static_cast<int>(rubanD.size())<pos+5)
                rubanD.resize(static_cast<size_t>(pos+30),BD);
            else if(pos<0 && static_cast<int>(rubanG.size())<pos+5)
                rubanG.resize(static_cast<size_t>(-pos+30),BG);
        }
        else if(op[i].first=="I")
        {
            if(op[i].second=="#")
            {
                string val;
                if(position_prec>=0)
                    val=rubanD[static_cast<size_t>(position_prec)];
                else
                    val=rubanG[static_cast<size_t>(-position_prec-1)];
                if(pos>=0)
                    rubanD[static_cast<size_t>(pos)]=val;
                else
                    rubanG[static_cast<size_t>(-pos-1)]=val;
            }
            else if(op[i].second=="")
            {
                if(pos>=0)
                    rubanD[static_cast<size_t>(pos)]=BD;
                else
                    rubanG[static_cast<size_t>(-pos-1)]=BG;
            }
            else
            {
                if(pos>=0)
                    rubanD[static_cast<size_t>(pos)]=op[i].second;
                else
                    rubanG[static_cast<size_t>(-pos-1)]=op[i].second;
            }
        }
    }

    string val;
    if(position_prec>=0)
        val=rubanD[static_cast<size_t>(position_prec)];
    else
        val=rubanG[static_cast<size_t>(-position_prec-1)];

    m_config=substitution(analyse_syntax_m_conf(m_fonction),
                          analyse_syntax_m_conf(m_config),
                          analyse_syntax_m_conf(m_config_resultante),
                          val);
}

string MTrans::name()
{
    return name_m_conf(m_fonc);
}

unsigned int MTrans::arite() const
{
    return 0;
}

bool MTrans::symbole_match(const string& e, const string& m_config)
{
    (void)m_config;
    return analyse_lex_symbole(symbole, e);
}
