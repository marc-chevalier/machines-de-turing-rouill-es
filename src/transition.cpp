#include "../include/m_fonc.h"

string Transition::get_m_config()
{
    return m_fonc;
}

string Transition::get_symbole()
{
    return symbole;
}

Transition::Transition(const string m_fonc_, const string symbole_, const string operation_, const string m_config_resultante_) :
m_fonc(m_fonc_), symbole(symbole_), operation(operation_), m_config_resultante(m_config_resultante_)
{}

Transition::Transition() :
m_fonc(""), symbole(""), operation(""), m_config_resultante("")
{}

Transition::~Transition()
{}
