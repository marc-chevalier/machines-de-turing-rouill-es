#include "../include/Parser.h"
#include<fstream>

Parser::Parser(string fileName_) :
fileName(fileName_), machine(nullptr), adj(vector<set<int>>(0)), correspondance(map<string, int>())
{}

Parser::Parser(const Parser& F) :
fileName(F.fileName), machine(F.machine), adj(F.adj), correspondance(F.correspondance)
{}

Parser& Parser::operator= (const Parser& other)
{
    Parser Temp(other);

    swap(Temp.fileName, this->fileName);
    swap(Temp.machine, this->machine);

    return *this;
}

Parser::~Parser()
{}

void Parser::parse()
{
    ifstream file(fileName);
    string q0;
    string Bg;
    string Bd;
    string qY;
    string qN;

    file>>q0;
    file>>Bg;
    file>>Bd;
    file>>qY;
    file>>qN;

    machine=new TM(q0, qY, qN, Bg, Bd);

    string type;
    string q;
    string q_;
    string a;
    string b;
    int m;
    int uid=0;

    while(file>>type>>q>>a>>q_>>b>>m)
    {
        if(type=="T")
        {
            string actions="I"+b+",";
            if(m==1)
                actions+="D";
            else if(m==-1)
                actions+="G";
            machine->addTable(new MTrans(q, a, actions, q_));
        }
        else if(type=="M")
        {
            string actions="I "+b+", ";
            if(m==1)
                actions+="D";
            else if(m==-1)
                actions+="G";
            machine->addTable(new MFonc(q, a, actions, q_));
        }
        if(correspondance.count(q)==0)
        {
            correspondance[q]=uid;
            ++uid;
        }
        if(correspondance.count(q_)==0)
        {
            correspondance[q_]=uid;
            ++uid;
        }
        if(correspondance[q]>=static_cast<int>(adj.size()))
            adj.resize(static_cast<size_t>(correspondance[q]+1), set<int>());
        if(q!=q_)
            adj[static_cast<size_t>(correspondance[q])].insert(correspondance[q_]);
    }
}

TM* Parser::getTM()
{
    if(machine==nullptr)
        parse();
    return machine;
}

int Parser::dfs(int sommet, int count, vector<int>& num, vector<int>& num_accessible, stack<int>& pile_accessible, vector<bool>& dans_pile, vector<int>& comp_connexe_pour_sommet, int& next_comp_connexe_id)
{
    num[static_cast<size_t>(sommet)] = count;
    num_accessible[static_cast<size_t>(sommet)] = count;
    ++count;
    pile_accessible.push(sommet);
    dans_pile[static_cast<size_t>(sommet)] = true;

    for(int fils : adj[static_cast<size_t>(sommet)])
    {
        if(num[static_cast<size_t>(fils)] == 0)
        {
            count = dfs(fils, count, num, num_accessible, pile_accessible, dans_pile, comp_connexe_pour_sommet, next_comp_connexe_id);
            if(num_accessible[static_cast<size_t>(fils)] < num_accessible[static_cast<size_t>(sommet)])
            {
                num_accessible[static_cast<size_t>(sommet)] = num_accessible[static_cast<size_t>(fils)];
            }
        }
        else if(dans_pile[static_cast<size_t>(fils)])
        {
            if(num[static_cast<size_t>(fils)] < num_accessible[static_cast<size_t>(sommet)])
            {
                num_accessible[static_cast<size_t>(sommet)] = num[static_cast<size_t>(fils)];
            }
        }
    }

    if(num_accessible[static_cast<size_t>(sommet)] == num[static_cast<size_t>(sommet)])
    {
        int sommet_pile;
        do
        {
            sommet_pile = pile_accessible.top();
            pile_accessible.pop();
            dans_pile[static_cast<size_t>(sommet_pile)] = false;
            comp_connexe_pour_sommet[static_cast<size_t>(sommet_pile)] = next_comp_connexe_id;
        }while(sommet_pile != sommet);
        next_comp_connexe_id++;
    }
    return count;
}

bool Parser::isMTR()
{
    if(machine==nullptr)
        parse();

    long unsigned int n=adj.size();
    vector<int> num(n);
    vector<int> num_accessible(n);
    stack<int> pile_accessible;
    int next_comp_connexe_id = 0;
    vector<int> comp_connexe_pour_sommet(n);
    vector<bool> dans_pile(n, false);

    int count = 1;
    for(long unsigned int j = 0; j<n; j++)
    {
        if(num[j] == 0)
            count = dfs(static_cast<int>(j), count, num, num_accessible, pile_accessible, dans_pile, comp_connexe_pour_sommet, next_comp_connexe_id);
    }

    vector<bool> comp_a_arete_entrante(static_cast<size_t>(next_comp_connexe_id + 1), false );
    for(long unsigned int j = 0; j < n; j++ )
    {
        int comp_connexe = comp_connexe_pour_sommet[j];
        for(int fils : adj[j])
        {
            if( comp_connexe_pour_sommet[static_cast<size_t>(fils)] != comp_connexe )
                comp_a_arete_entrante[static_cast<size_t>(comp_connexe_pour_sommet[static_cast<size_t>(fils)])] = true;
        }
    }

    map<int, int> population;

    for(unsigned int i=0;i<n;++i)
        population[comp_connexe_pour_sommet[i]]++;

    for(pair<int, int> e : population)
        if(e.second>1)
            return false;

    return true;
}
