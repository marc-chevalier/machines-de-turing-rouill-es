#include "../include/fonction.h"

void print(const vector<string>& e)
{
    cout<<"$";
    for(unsigned int i=0;i<e.size();++i)
        cout<<e[i]<<"|";
    cout<<"$";
    cout<<endl;
}

void print(const vector<pair<string,string> >& e)
{
    cout<<"$";
    for(unsigned int i=0;i<e.size();++i)
        cout<<e[i].first<<","<<e[i].second<<":";
    cout<<"$";
    cout<<endl;
}

void print(const vector<string>& e, ofstream& save)
{
    save<<"$"<<endl;
    for(unsigned int i=0;i<e.size();++i)
    {
        save<<e[i]<<"|";
        if(i%50==49)
            save<<endl;
    }
    save<<endl<<"$";
    save<<endl;
}

void print(const vector<pair<string,string> >& e, ofstream& save)
{
    save<<"$"<<endl;
    for(unsigned int i=0;i<e.size();i++)
    {
        save<<e[i].first<<","<<e[i].second<<"|";
        if(i%25==24)
            save<<endl;
    }
    save<<"$";
    save<<endl;
}

bool is_m_function(const string& m_conf)
{
    bool arg=false;
    int niveau=0;
    long unsigned int n=m_conf.size();

    for(unsigned int i=0;i<n;++i)
    {
        if(m_conf[i]=='(')
        {
            if(i==0)
                return false;
            arg=true;
            ++niveau;
        }
        else if(m_conf[i]==')')
            --niveau;
    }
    return (niveau==0 && arg);
}

unsigned int arite_m_conf(const string& m_conf)
{
    unsigned int comp=0;
    int niveau=0;
    long unsigned int n=m_conf.size();

    for(unsigned int i=0;i<n;++i)
    {
        if(m_conf[i]=='(')
            ++niveau;
        else if(m_conf[i]==')')
            --niveau;
        else if(m_conf[i]==',' && niveau==1)
            ++comp;
    }
    return comp+1;
}

string name_m_conf(const string& m_conf)
{
    string sortie;
    long unsigned int n=m_conf.size();

    for(unsigned int i=0;i<n;++i)
    {
        if(m_conf[i]=='(')
            break;
        sortie+=m_conf[i];
    }
    return sortie;
}

vector<string> analyse_syntax_m_conf(const string& m_conf)
{
    vector<string> sortie(0);
    string work;
    unsigned int i=0;
    int niveau=1;
    long unsigned int n=m_conf.size();

    work.clear();
    while(i<n)
    {
        if(m_conf[i]=='(')
            break;
        work+=m_conf[i];
        ++i;
    }
    sortie.push_back(work);
    ++i;

    while(i<n)
    {
        work.clear();
        while((m_conf[i]!=',' && m_conf[i]!=')') || niveau>1)
        {
            if(m_conf[i]=='(')
                ++niveau;
            else if(m_conf[i]==')')
                --niveau;
            work+=m_conf[i];
            ++i;
        }
        ++i;
        sortie.push_back(work);
    }
    return sortie;
}

vector<string> analyse_syntax_symbole(const string& symbole)
{
    if(symbole.size()==0)///Si tous les symboles sont acceptes
        return vector<string>(0);

    vector<string> sortie(0);///Sinon on fait une analyse syntaxique similaires a celle des m_configurations
    string work;
    unsigned int i=0;
    int niveau=1;
    long unsigned int n=symbole.size();

    while(i<n)
    {
        if(symbole[i]=='(')
            break;
        work=work+symbole[i];
        ++i;
    }
    if(work.size()>0)
        sortie.push_back(work);
    ++i;

    while(i<n)
    {
        work.clear();
        while((symbole[i]!=',' && symbole[i]!=')') || niveau>1)
        {
            if(symbole[i]=='(')
                ++niveau;
            else if(symbole[i]==')')
                --niveau;
            work+=symbole[i];
            ++i;
        }
        ++i;
        sortie.push_back(work);
    }
    return sortie;
}

bool analyse_lex_symbole(const string& filtrage, const string& symbole)
{
    if(filtrage.size()==0)
        return true;
    vector<string> syntaxe=analyse_syntax_symbole(filtrage);
    long unsigned int n=syntaxe.size();

    if(n==0)
        return true;
    if(syntaxe[0]=="non")
    {
        for(unsigned int i=1;i<n;++i)
            if(syntaxe[i]==symbole)
                return false;
        return true;
    }
    for(unsigned int i=0;i<n;++i)
        if(syntaxe[i]==symbole)
            return true;
    return false;

}

vector<pair<string,string> > analyse_syntax_operation(const string& operations, const vector<string>& m_conf, const vector<string>& m_fonc)
{
    vector<pair<string,string> > sortie;
    string work;
    pair<string,string> w2;
    unsigned int i_=0;
    long unsigned int n=operations.size();

    while(i_<n)
    {
        work.clear();
        while(i_<n)
        {
            if(operations[i_]==',')
                break;
            work+=operations[i_];
            ++i_;
        }
        w2.first=work;
        sortie.push_back(w2);
        ++i_;
    }

    long unsigned int m=sortie.size();
    long unsigned int o=m_fonc.size();

    for(unsigned int i=0;i<m;++i)
        if(sortie[i].first[0]=='I')
        {
            for(unsigned int j=1;j<sortie[i].first.size();++j)
                sortie[i].second=sortie[i].second+sortie[i].first[j];
            sortie[i].first="I";
        }

    for(unsigned int i=0;i<m;++i)
        if(sortie[i].first=="I")
            for(unsigned int j=1;j<o;++j)
                if(sortie[i].second==m_fonc[j])
                {
                    sortie[i].second=m_conf[j];
                    break;
                }
    return sortie;
}

string substitution(const vector<string>& m_fonc, const vector<string>& m_conf, const vector<string>& result, const string& sym)
{
    /*print(m_fonc);
    print(m_conf);
    print(result);
    cout<<sym<<endl;*/

    long unsigned int n=result.size();

    if(n==1)
        for(unsigned int i=0;i<m_fonc.size();++i)
            if(result[0]==m_fonc[i])
                return m_conf[i];

    if(n==1)
        return result[0];

    string sortie=result[0]+"(";
    bool found=false;
    for(unsigned int i=1;i<n;++i)
    {
        if(is_m_function(result[i]))
            sortie+=substitution(m_fonc,m_conf,analyse_syntax_m_conf(result[i]),sym);
        else
        {
            found=false;
            for(unsigned int j=0;j<m_fonc.size();++j)
            {
                if(result[i]=="#")
                {
                    found=true;
                    sortie+=sym;
                    break;
                }
                else if(result[i]==m_fonc[j])
                {
                    found=true;
                    sortie+=m_conf[j];
                    break;
                }
            }
            if(not found)
                sortie+=result[i];
        }
        sortie+=",";
    }
    sortie[sortie.size()-1]=')';
   // cout<<"!!"<<sortie<<endl;
    return sortie;
}

string substitution_symbole(const vector<string>& m_fonc, const vector<string>& m_conf, const vector<string>& sym)
{
    string sortie;
    bool found=false;
    bool it_1=false;
    int k=0;
    long unsigned int n=sym.size();
    long unsigned int m=m_fonc.size();

    if(n==0)
        return "";

    if(sym[0]=="non")
    {
        sortie+="non";
        k=1;
    }

    sortie+='(';

    for(unsigned int i=static_cast<unsigned int>(k);i<n;++i)
    {
        found=false;
        for(unsigned int j=0;j<m;++j)
            if(sym[i]==m_fonc[j])
            {
                sortie+=m_conf[j];
                found=true;
                break;
            }
        if(not found)
            sortie+=sym[i];
        if(not it_1)
            sortie+=",";
        it_1=true;
    }
    sortie[sortie.size()-1]=')';
    return sortie;
}

