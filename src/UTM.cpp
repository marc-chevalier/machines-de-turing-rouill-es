#include "../include/UTM.h"

vector<string> UTM::newUTMTape()
{
    vector<string> ruban(200,"o");
    ruban[0]="@";
    ruban[1]="@";
    ruban[2]=";";///Correction de l'erreur à propos des ';' de la DS.
    ruban[4]="C";
    ruban[6]="A";
    ruban[8]="C";
    ruban[10]="C";
    ruban[12]="B";
    ruban[14]="D";
    ruban[16]="C";
    ruban[18]="A";
    ruban[20]="A";
    ruban[22]=";";
    ruban[24]="C";
    ruban[26]="A";
    ruban[28]="A";
    ruban[30]="C";
    ruban[32]="C";
    ruban[34]="D";
    ruban[36]="C";
    ruban[38]="A";
    ruban[40]="A";
    ruban[42]="A";
    ruban[44]=";";
    ruban[46]="C";
    ruban[48]="A";
    ruban[50]="A";
    ruban[52]="A";
    ruban[54]="C";
    ruban[56]="C";
    ruban[58]="B";
    ruban[60]="B";
    ruban[62]="D";
    ruban[64]="C";
    ruban[66]="A";
    ruban[68]="A";
    ruban[70]="A";
    ruban[72]="A";
    ruban[74]=";";
    ruban[76]="C";
    ruban[78]="A";
    ruban[80]="A";
    ruban[82]="A";
    ruban[84]="A";
    ruban[86]="C";
    ruban[88]="C";
    ruban[90]="D";
    ruban[92]="C";
    ruban[94]="A";
    ruban[96]="+";

    return ruban;
}


TM* UTM::newUTM() //executer avec     machine->exec(ruban,2);
{
    Transition* f_1=new MFonc("f(EE,BB,aa)","(@)","G","f1(EE,BB,aa)");///Find
    Transition* f_2=new MFonc("f(EE,BB,aa)","non(@,o)","G","f(EE,BB,aa)");
    Transition* f_3=new MFonc("f(EE,BB,aa)","(o)","G","f(EE,BB,aa)");
    Transition* f1_1=new MFonc("f1(EE,BB,aa)","(aa)","N","EE");
    Transition* f1_2=new MFonc("f1(EE,BB,aa)","(o)","D","f2(EE,BB,aa)");
    Transition* f1_3=new MFonc("f1(EE,BB,aa)","non(o,aa)","D","f1(EE,BB,aa)");
    Transition* f2_1=new MFonc("f2(EE,BB,aa)","(aa)","N","EE");
    Transition* f2_2=new MFonc("f2(EE,BB,aa)","(o)","D","BB");
    Transition* f2_3=new MFonc("f2(EE,BB,aa)","non(o,aa)","D","f1(EE,BB,aa)");

    Transition* e_1=new MFonc("e(EE,BB,aa)","","","f(e1(EE,BB,aa),BB,aa)");///Erase
    Transition* e1_1=new MFonc("e1(EE,BB,aa)","","E","EE");
    Transition* e_2=new MFonc("e(BB,aa)","","","e(e(BB,aa),BB,aa)");

    Transition* pe_1=new MFonc("pe(EE,bb)","","","f(pe1(EE,bb),EE,@)");///Print at end
    Transition* pe1_1=new MFonc("pe1(EE,bb)","non(o)","D,D","pe1(EE,bb)");
    Transition* pe1_2=new MFonc("pe1(EE,bb)","(o)","Ibb","EE");

    Transition* l_1=new MFonc("l(EE)","","G","EE");///Find and copy
    Transition* r_1=new MFonc("r(EE)","","D","EE");
    Transition* f0_1=new MFonc("f'(EE,BB,aa)","","","f(l(EE),BB,aa)");
    Transition* f00_1=new MFonc("f''(EE,BB,aa)","","","f(r(EE),BB,aa)");
    Transition* c_1=new MFonc("c(EE,BB,aa)","","","f'(c1(EE),BB,aa)");
    Transition* c1_1=new MFonc("c1(EE)","","","pe(EE,#)");

    Transition* ce_1=new MFonc("ce(EE,BB,aa)","","","c(e(EE,BB,aa),BB,aa)");///Copy erase
    Transition* ce_2=new MFonc("ce(BB,aa)","","","ce(ce(BB,aa),BB,aa)");

    Transition* re_1=new MFonc("re(EE,BB,aa,bb)","","","f(re1(EE,BB,aa,bb),BB,aa)");///Replace
    Transition* re1_1=new MFonc("re1(EE,BB,aa,bb)","","E,Ibb","EE");
    Transition* re_2=new MFonc("re(BB,aa,bb)","","E,Ibb","re(re(BB,aa,bb),BB,aa,bb)");

    Transition* cr_1=new MFonc("cr(EE,BB,aa)","","","c(re(EE,BB,aa,$a$),BB,aa)");
    Transition* cr_2=new MFonc("cr(BB,aa)","","","cr(ce(BB,aa),re(BB,$a$,aa),aa)");

    Transition* cp_1=new MFonc("cp(EE1,UU,EE,aa,bb)","","","f'(cp1(EE1,UU,bb),f(UU,EE,bb),aa)");///Compare
    Transition* cp1_1=new MFonc("cp1(EE,UU,bb)","","","f'(cp2(EE,UU,#),UU,bb)");
    Transition* cp2_1=new MFonc("cp2(EE,UU,gg)","(gg)","","EE");
    Transition* cp2_2=new MFonc("cp2(EE,UU,gg)","non(gg,o)","","UU");

    Transition* cpe_1=new MFonc("cpe(EE1,UU,EE,aa,bb)","","","cp(e(e(EE1,EE,bb),EE,aa),UU,EE,aa,bb)");///Compare erase
    Transition* cpe_2=new MFonc("cpe(UU,EE,aa,bb)","","","cpe(cpe(UU,EE,aa,bb),UU,EE,aa,bb)");

    Transition* q_1=new MFonc("q(EE)","non(o)","D","q(EE)");
    Transition* q_2=new MFonc("q(EE)","(o)","D","q1(EE)");
    Transition* q1_1=new MFonc("q1(EE)","non(o)","D","q(EE)");
    Transition* q1_2=new MFonc("q1(EE)","(o)","","EE");
    Transition* q_3=new MFonc("q(EE,aa)","","","q(q1(EE,aa))");
    Transition* q1_3=new MFonc("q1(EE,aa)","(aa)","","EE");
    Transition* q1_4=new MFonc("q1(EE,aa)","non(aa)","G","q1(EE,aa)");

    Transition* pe2_1=new MFonc("pe2(EE,aa,bb)","","","pe(pe(EE,bb),aa)");
    Transition* ce2_1=new MFonc("ce2(BB,aa,bb)","","","ce(ce(BB,bb),aa)");
    Transition* ce3_1=new MFonc("ce3(BB,aa,bb,gg)","","","ce(ce2(BB,bb,gg),aa)");
    Transition* ce4_1=new MFonc("ce4(BB,aa,bb,gg,dd)","","","ce(ce3(BB,bb,gg,dd),aa)");
    Transition* ce5_1=new MFonc("ce5(BB,aa,bb,gg,dd,ee)","","","ce(ce4(BB,bb,gg,dd,ee),aa)");

    Transition* e_3=new MFonc("e(EE)","(@)","D","e1(EE)");
    Transition* e_4=new MFonc("e(EE)","non(@)","G","e(EE)");
    Transition* e1_3=new MFonc("e1(EE)","non(o)","D,E,D","e1(EE)");
    Transition* e1_4=new MFonc("e1(EE)","(o)","","EE");

    Transition* con_1=new MFonc("con(EE,aa)","non(A,o)","D,D","con(EE,aa)");///Configuration
    Transition* con_2=new MFonc("con(EE,aa)","(A)","G,Iaa,D","con1(EE,aa)");
    Transition* con1_1=new MFonc("con1(EE,aa)","(A)","D,Iaa,D","con1(EE,aa)");
    Transition* con1_2=new MFonc("con1(EE,aa)","(C)","D,Iaa,D","con2(EE,aa)");
    Transition* con1_3=new MFonc("con1(EE,aa)","(o)","IC,D,Iaa,D,D,D","EE");
    Transition* con2_1=new MFonc("con2(EE,aa)","(B)","D,Iaa,D","con2(EE,aa)");
    Transition* con2_2=new MFonc("con2(EE,aa)","non(B)","D,D","EE");

    Transition* b_1=new MTrans("b","","","f(b1,b1,+)");///Begin
    Transition* b1_1=new MTrans("b1","","D,D,I:,D,D,IC,D,D,IA","anf");///Incrire CA.

    Transition* anf_1=new MTrans("anf","","","q(anf1,:)");
    Transition* anf1_1=new MTrans("anf1","","","con(fom,y)");
    Transition* fom_1=new MTrans("fom","(;)","D,Iz,G","con(fmp,x)");
    Transition* fom_2=new MTrans("fom","(z)","G,G","fom");
    Transition* fom_3=new MTrans("fom","non(;,z)","G","fom");
    Transition* fmp=new MTrans("fmp","","","cpe(e(e(anf,y),x),sim,x,y)");

    Transition* sim_1=new MTrans("sim","","","f'(sim1,sim1,z)");///Simulate
    Transition* sim1_1=new MTrans("sim1","","","con(sim2,o)");
    Transition* sim2_1=new MTrans("sim2","(A)","","sim3");
    Transition* sim2_2=new MTrans("sim2","non(A)","G,Iu,D,D,D","sim2");
    Transition* sim3_1=new MTrans("sim3","non(A)","G,Iy","e(mf,z)");
    Transition* sim3_2=new MTrans("sim3","(A)","G,Iy,D,D,D","sim3");

    Transition* mf_1=new MTrans("mf","","","q(mf1,:)");
    Transition* mf1_1=new MTrans("mf1","non(A)","D,D","mf1");
    Transition* mf1_2=new MTrans("mf1","(A)","G,G,G,G","mf2");
    Transition* mf2_1=new MTrans("mf2","(B)","D,Ix,G,G,G","mf2");
    Transition* mf2_2=new MTrans("mf2","(:)","","mf4");
    Transition* mf2_3=new MTrans("mf2","(C)","D,Ix,G,G,G","mf3");
    Transition* mf3_1=new MTrans("mf3","non(:)","D,Iv,G,G,G","mf3");
    Transition* mf3_2=new MTrans("mf3","(:)","","mf4");
    Transition* mf4_1=new MTrans("mf4","","","con(l(l(mf5)),)");
    Transition* mf5_1=new MTrans("mf5","non(o)","D,Iw,D","mf5");
    Transition* mf5_2=new MTrans("mf5","(o)","I:","sh");

    Transition* sh_1=new MTrans("sh","","","f(sh1,inst,u)");///Show
    Transition* sh1_1=new MTrans("sh1","","G,G,G","sh2");
    Transition* sh2_1=new MTrans("sh2","(C)","D,D,D,D","sh3");///
    Transition* sh2_2=new MTrans("sh2","non(C)","","inst");///
    Transition* sh3_1=new MTrans("sh3","(B)","D,D","sh4");///
    Transition* sh3_2=new MTrans("sh3","non(B)","","inst");///
    Transition* sh4_1=new MTrans("sh4","(B)","D,D","sh5");///
    Transition* sh4_2=new MTrans("sh4","non(B)","","pe2(inst,0,:)");///
    Transition* sh5_1=new MTrans("sh5","(B)","","inst");///
    Transition* sh5_2=new MTrans("sh5","non(B)","","pe2(inst,1,:)");///

    Transition* inst_1=new MTrans("inst","","","q(l(inst1),u)");

    Transition* inst1_1=new MTrans("inst1","(G)","D,E","inst1G");
    Transition* inst1_2=new MTrans("inst1","(D)","D,E","inst1D");
    Transition* inst1_3=new MTrans("inst1","(N)","D,E","inst1N");

    Transition* inst1_4=new MTrans("inst1G","","","ce5(ov,v,y,x,u,w)");
    Transition* inst1_5=new MTrans("inst1D","","","ce5(ov,v,x,u,y,w)");
    Transition* inst1_6=new MTrans("inst1N","","","ce5(ov,v,x,y,u,w)");

    Transition* ov=new MTrans("ov","","","e(anf)");

    TM* machine=new TM("b", "", "", "og", "od");

    machine->addTable(f_1);
    machine->addTable(f_2);
    machine->addTable(f_3);
    machine->addTable(f1_1);
    machine->addTable(f1_2);
    machine->addTable(f1_3);
    machine->addTable(f2_1);
    machine->addTable(f2_2);
    machine->addTable(f2_3);
    machine->addTable(e_1);
    machine->addTable(e1_1);
    machine->addTable(e_2);
    machine->addTable(pe_1);
    machine->addTable(pe1_1);
    machine->addTable(pe1_2);
    machine->addTable(l_1);
    machine->addTable(r_1);
    machine->addTable(f0_1);
    machine->addTable(f00_1);
    machine->addTable(c_1);
    machine->addTable(c1_1);
    machine->addTable(ce_1);
    machine->addTable(ce_2);
    machine->addTable(re_1);
    machine->addTable(re1_1);
    machine->addTable(re_2);
    machine->addTable(cr_1);
    machine->addTable(cr_2);
    machine->addTable(cp_1);
    machine->addTable(cp1_1);
    machine->addTable(cp2_1);
    machine->addTable(cp2_2);
    machine->addTable(cpe_1);
    machine->addTable(cpe_2);
    machine->addTable(q_1);
    machine->addTable(q_2);
    machine->addTable(q1_1);
    machine->addTable(q1_2);
    machine->addTable(q_3);
    machine->addTable(q1_3);
    machine->addTable(q1_4);
    machine->addTable(pe2_1);
    machine->addTable(ce2_1);
    machine->addTable(ce3_1);
    machine->addTable(ce4_1);
    machine->addTable(ce5_1);
    machine->addTable(e_3);
    machine->addTable(e_4);
    machine->addTable(e1_3);
    machine->addTable(e1_4);
    machine->addTable(con_1);
    machine->addTable(con_2);
    machine->addTable(con1_1);
    machine->addTable(con1_2);
    machine->addTable(con1_3);
    machine->addTable(con2_1);
    machine->addTable(con2_2);
    machine->addTable(b_1);
    machine->addTable(b1_1);
    machine->addTable(anf_1);
    machine->addTable(anf1_1);
    machine->addTable(fom_1);
    machine->addTable(fom_2);
    machine->addTable(fom_3);
    machine->addTable(fmp);
    machine->addTable(sim_1);
    machine->addTable(sim1_1);
    machine->addTable(sim2_1);
    machine->addTable(sim2_2);
    machine->addTable(sim3_1);
    machine->addTable(sim3_2);
    machine->addTable(mf_1);
    machine->addTable(mf1_1);
    machine->addTable(mf1_2);
    machine->addTable(mf2_1);
    machine->addTable(mf2_2);
    machine->addTable(mf2_3);
    machine->addTable(mf3_1);
    machine->addTable(mf3_2);
    machine->addTable(mf4_1);
    machine->addTable(mf5_1);
    machine->addTable(mf5_2);
    machine->addTable(sh_1);
    machine->addTable(sh1_1);
    machine->addTable(sh2_1);
    machine->addTable(sh2_2);
    machine->addTable(sh3_1);
    machine->addTable(sh3_2);
    machine->addTable(sh4_1);
    machine->addTable(sh4_2);
    machine->addTable(sh5_1);
    machine->addTable(sh5_2);
    machine->addTable(inst_1);
    machine->addTable(inst1_1);
    machine->addTable(inst1_2);
    machine->addTable(inst1_3);
    machine->addTable(inst1_4);
    machine->addTable(inst1_5);
    machine->addTable(inst1_6);
    machine->addTable(ov);

    return machine;
}
