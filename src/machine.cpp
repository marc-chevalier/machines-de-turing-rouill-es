#include "../include/machine.h"
#define EVER ;;
#define IT_MAX 1000000

TM::TM(string q0_, string qY_, string qN_, string BG_, string BD_) :
BG(BG_), BD(BD_), qY(qY_), qN(qN_), q0(q0_), table (vector<Transition*>()), rubanG(vector<string>()), rubanD(vector<string>()), pos(0), m_configuration("")
{}

TM::~TM()
{
    for(Transition* t : table)
        delete t;
}

string TM::ruban(int e) const
{
    return e>=0 ? rubanD[static_cast<size_t>(e)] : rubanG[static_cast<size_t>(-e-1)];
}

bool TM::exec(vector<string> rubanG_, vector<string> rubanD_)
{
    return exec(rubanG_, rubanD_, 0);
}

bool TM::exec(vector<string> rubanG_, vector<string> rubanD_, int p)
{
    rubanG=rubanG_;
    rubanD=rubanD_;
    pos=p;
    return exec();
}

bool TM::exec()
{
    int it=0;
    time_t t1, t2;
    double dif;
    long unsigned int n=table.size();
    time(&t1);

    if(pos>=0 && static_cast<int>(rubanD.size())<pos)
        rubanD.resize(static_cast<size_t>(pos+10));

    bool found=false;
    m_configuration=q0;
    unsigned int t;

    ofstream save("save_file.txt");

    for(EVER)
    {
        found=false;
        if(is_m_function(m_configuration))
        {
            for(unsigned int i=0;i<n;++i)
            {
                if(table[i]->name()==name_m_conf(m_configuration) && table[i]->arite()==arite_m_conf(m_configuration) && table[i]->symbole_match(ruban(pos),m_configuration))
                {
                    t=i;
                    found=true;
                    break;
                }
            }
        }
        else
        {
            for(unsigned int i=0;i<n;++i)
            {
                if(table[i]->name()==m_configuration && table[i]->symbole_match(ruban(pos),m_configuration))
                {
                    t=i;
                    found=true;
                    break;
                }
            }
        }

        /**cout<<arite_m_conf(m_configuration)<<" "<<found<<endl;
        cout<<table[16]->arite()<<" "<<table[16]->name()<<" "<<arite_m_conf(table[16]->get_m_config())<<endl;
        cout<<table[16]->symbole_match(ruban[pos],m_configuration)<<endl;**/
        if(not found)
            break;

        //cout<<ruban.size()<<endl;
        //save<<rubanG.size()<<" "<<rubanD.size()<<endl;

        table[t]->exec(rubanG, rubanD, pos, m_configuration, BG, BD, table[t]->get_m_config());

        /*cout<<rubanG.size()<<endl;
        cout<<rubanD.size()<<endl;
        print(rubanG);
        print(rubanD);
        ///cout<<found<<" "<<t<<" "<<table[t]->get_m_config()<<endl;
        ///cout<<is_m_function(m_configuration)<<" "<<m_configuration<<" "<<table[t]->name()<<" "<<table[t]->symbole_match(ruban[pos],m_configuration)<<endl;
        cout<<pos<<" "<<ruban(pos)<<endl;
        cout<<it<<" "<<m_configuration<<endl;
        cout<<endl<<endl<<endl<<endl<<endl;*/
        save<<rubanG.size()<<" "<<rubanD.size()<<endl;
        print(rubanG,save);
        print(rubanD,save);
        ///cout<<found<<" "<<t<<" "<<table[t]->get_m_config()<<endl;
        ///cout<<is_m_function(m_configuration)<<" "<<m_configuration<<" "<<table[t]->name()<<" "<<table[t]->symbole_match(ruban[pos],m_configuration)<<endl;

        save<<pos<<" "<<ruban(pos)<<endl;
        save<<it<<" "<<m_configuration<<endl;
        save<<endl<<endl<<endl<<endl<<endl;

        /*if(it>IT_MAX)
            break;
        if(it%1000==0)
            cout<<it/1000<<" ";*/
        ++it;

        if(m_configuration==qY || m_configuration==qN)
            break;
    }

    time(&t2);
    dif = difftime (t2,t1);
    save<<"Time : "<<dif<<endl;
    save<<"it : "<<it<<endl;
    for(unsigned int i=0;i<rubanG.size();i++)
    {
        save<<rubanG[i]<<"|";
        if(i%50==49)
            save<<endl;
    }
    save<<endl<<endl;
    for(unsigned int i=0;i<rubanD.size();i++)
    {
        save<<rubanD[i]<<"|";
        if(i%50==49)
            save<<endl;
    }
    save<<endl;
    save<<pos<<" "<<ruban(pos)<<endl;
    save<<m_configuration<<endl;
    save.close();
    return m_configuration==qY;
}

vector<string> TM::getRubanD(void) const
{
    return rubanD;
}

vector<string> TM::getRubanG(void) const
{
    return rubanG;
}

void TM::setRubanD(vector<string>& e)
{
    rubanD=e;
}

void TM::setRubanG(vector<string>& e)
{
    rubanG=e;
}

vector<Transition*> TM::getTable(void) const
{
    return table;
}

void TM::setTable(vector<Transition*> e)
{
    table=e;
}

void TM::addTable(Transition* e)
{
    table.push_back(e);
}

void TM::addTable(vector<Transition*>& e)
{
    for(unsigned int i=0;i<e.size();++i)
        table.push_back(e[i]);
}

string TM::getBG(void) const
{
    return BG;
}

string TM::getBD(void) const
{
    return BD;
}

void TM::setBD(string e)
{
    BD=e;
}

void TM::setBG(string e)
{
    BD=e;
}

string TM::getQ0(void) const
{
    return q0;
}

void TM::setQ0(string e)
{
    q0=e;
}

int TM::getPos(void) const
{
    return pos;
}

void TM::setPos(int e)
{
    pos=e;
}
