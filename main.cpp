#include "include/UTM.h"
#include "include/Parser.h"

vector<string> getRuban(string fileName);

vector<string> getRuban(string fileName)
{
    vector<string> sortie;
    string w;
    ifstream file(fileName);
    while(file>>w)
        sortie.push_back(w);

    return sortie;
}

int main(int argc, char* argv[])
{

    /**
       /\     Respecter scrupuleusement la syntaxe que Turing et moi avons imposée.  /\
      /  \                                                                          /  \
     / !! \                                                                        / !! \
    /______\                                                                      /______\
       ||                                                                            ||
       ||                                                                            ||
       ||                                                                            ||
       ||                                                                            ||
       Ceux qui ne respecterons pas cette regle seront puni sommairement d'une boucle
       infinie ou plus probablement d'une erreur memoire suivi d'un plantage immédiat.
    **/
    /*TM* machine=UTM::newUTM();
    vector<string> rubanD(UTM::newUTMTape());

    machine->exec(vector<string>(0), rubanD, 2);
    delete machine;*/

    if(argc<3)
    {
        cerr<<"Pas assez d'arguments"<<endl;
        exit(EXIT_FAILURE);
    }
    Parser p(argv[1]);
    p.parse();
    TM* machine = p.getTM();
    /*if(!p.isMTR())
    {
        cerr<<"Cette machine n'est pas rouillée"<<endl;
    }*/
    vector<string> rubanD=getRuban(argv[2]);
    vector<string> rubanG(0);

    if(argc>=4)
        if(argv[3][0]=='1')
        {
            rubanD.push_back("µD");
            rubanG.push_back("µG");
        }

    /*for(string s : rubanD)
    {
        cout<<s<<endl;
    }*/

    if(machine->exec(rubanG,rubanD))
        cout<<"Ca marche !"<<endl;
    else
        cout<<"Ca ne marche pas !"<<endl;

    return 0;
}
