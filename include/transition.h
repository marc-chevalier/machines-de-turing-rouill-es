#ifndef TRANSITION_H_INCLUDED
#define TRANSITION_H_INCLUDED

#include "fonction.h"

class Transition
{
    public:
    Transition();
    Transition(const string m_fonc_e, const string symbole_e, const string operation_e, const string m_config_resultante_e);
    virtual ~Transition();
    virtual void exec(vector<string>& rubanG, vector<string>& rubanD, int& pos, string& m_config, const string& BG, const string& BD,const string& m_fonc)=0;
    virtual string name()=0;
    virtual unsigned int arite() const=0;
    string get_m_config();
    string get_symbole();
    virtual bool symbole_match(const string& e, const string& m_config)=0;

    protected:
    string m_fonc;
    string symbole;
    string operation;
    string m_config_resultante;
};

#endif // TRANSITION_H_INCLUDED
