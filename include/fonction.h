#ifndef FONCTION_H_INCLUDED
#define FONCTION_H_INCLUDED

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>

using namespace std;

void print(const vector<string>& e);
void print(const vector<pair<string,string> >& e);
void print(const vector<string>& e, ofstream& save);
void print(const vector<pair<string,string> >& e, ofstream& save);
bool is_m_function(const string& m_conf) __attribute__((pure));
unsigned int arite_m_conf(const string& m_conf) __attribute__((pure));
string name_m_conf(const string& m_conf);
vector<string> analyse_syntax_m_conf(const string& m_conf);
vector<string> analyse_syntax_symbole(const string& symbole);
bool analyse_lex_symbole(const string& filtrage, const string& symbole);
vector<pair<string,string> > analyse_syntax_operation(const string& operations, const vector<string>& m_conf, const vector<string>& m_fonc);
string substitution(const vector<string>& m_fonc, const vector<string>& m_conf, const vector<string>& result, const string& sym);
string substitution_symbole(const vector<string>& m_fonc, const vector<string>& m_conf, const vector<string>& sym);


#endif // FONCTION_H_INCLUDED
