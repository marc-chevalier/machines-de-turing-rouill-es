#ifndef PARSER_H
#define PARSER_H

#include "machine.h"
#include "m_fonc.h"
#include "m_trans.h"
#include<map>
#include<set>
#include<stack>

class Parser
{
public:
    Parser(string fileName);
    Parser(const Parser& F);
    Parser& operator= (const Parser& other);
    ~Parser();
    void parse();
    TM* getTM();
    bool isMTR();

private:
    int dfs( int sommet, int count, vector<int>& num, vector<int>& num_accessible, stack<int>& pile_accessible, vector<bool>& dans_pile, vector<int>& comp_connexe_pour_sommet, int& next_comp_connexe_id);

    string fileName;
    TM* machine;
    vector<set<int>> adj;
    map<string, int> correspondance;
};

#endif // PARSER_H
