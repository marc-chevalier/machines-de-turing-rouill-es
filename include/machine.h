#ifndef MACHINE_H_INCLUDED
#define MACHINE_H_INCLUDED

#include <ctime>
#include "transition.h"

class Transition;
class MFonc;
class MTrans;

class TM
{
    public:
    TM(string q0, string qY, string qN, string BG, string BD);
    ~TM();
    bool exec(vector<string> rubanG_, vector<string> rubanD_);
    bool exec(vector<string> rubanG_, vector<string> rubanD_, int p);
    bool exec();
    vector<string> getRubanG(void) const;
    vector<string> getRubanD(void) const;
    void setRubanD(vector<string>& e);
    void setRubanG(vector<string>& e);
    vector<Transition*> getTable(void) const;
    void setTable(vector<Transition*> e);
    void addTable(Transition* e);
    void addTable(vector<Transition*>& e);
    string getBD(void) const;
    void setBD(string e);
    string getBG(void) const;
    void setBG(string e);
    string getQ0(void) const ;
    void setQ0(string e);
    int getPos(void) const __attribute__((pure));
    void setPos(int e);

    protected:
    string ruban(int pos) const;

    string BG;
    string BD;
    string qY;
    string qN;
    string q0;
    vector<Transition*> table;
    vector<string> rubanG;
    vector<string> rubanD;
    int pos;
    string m_configuration;
};

#endif // MACHINE_H_INCLUDED
