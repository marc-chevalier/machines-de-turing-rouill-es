#ifndef UTM_H
#define UTM_H

#include "fonction.h"
#include "machine.h"
#include "m_trans.h"
#include "m_fonc.h"

class UTM
{
public:
    static TM* newUTM();
    static vector<string> newUTMTape();
};

#endif // UTM_H
