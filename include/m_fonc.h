#ifndef M_CONF_H_INCLUDED
#define M_CONF_H_INCLUDED

#include "transition.h"

class MFonc : public Transition
{
public:
    MFonc(const string m_fonc_e, const string symbole_e, const string operation_e, const string m_config_resultante_e);
    virtual void exec(vector<string>& rubanG, vector<string>& rubanD, int& pos, string& m_config, const string& BG, const string& BD, const string& m_fonc);
    virtual string name();
    virtual unsigned int arite() const __attribute__((pure));
    virtual bool symbole_match(const string& e, const string& m_config);
};


#endif // M_CONF_H_INCLUDED
