CC=g++
C11= -std=c++0x
FLAGSBASE= -O2 -W -Wextra -Wcast-qual -Wcast-align -Wfloat-equal -Wshadow -Wpointer-arith -Wunreachable-code -Wchar-subscripts -Wcomment -Wformat -Werror-implicit-function-declaration -Wmain -Wmissing-braces -Wparentheses -Wsequence-point -Wreturn-type -Wswitch -Wuninitialized -Wreorder -Wundef -Wwrite-strings -Wsign-compare -Wmissing-declarations 
NAZIBASE= $(FLAGSBASE) -pedantic -Wconversion -Wmissing-noreturn -Wold-style-cast -Weffc++ -Wall -Wunused -Wsign-conversion -Wunused -Wstrict-aliasing -Wstrict-overflow -Wconversion -Wdisabled-optimization
ifeq ($(CC), g++)
    NAZI= $(NAZIBASE) -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-attribute=pure -Wlogical-op -Wunsafe-loop-optimizations
else
    NAZI=$(NAZIBASE)
endif
CFLAGS=$(NAZI)
LDFLAGS= 
SOLVEURS=
EXEC=setup TM

all: $(EXEC) 

debug: CFLAGS = $(NAZI) -D DEBUG -g
debug: FLAGSBASE = -O2 -W -Wextra -Wcast-qual -Wcast-align -Wfloat-equal -Wshadow -Wpointer-arith -Wunreachable-code -Wchar-subscripts -Wcomment -Wformat -Werror-implicit-function-declaration -Wmain -Wmissing-braces -Wparentheses -Wsequence-point -Wreturn-type -Wswitch -Wuninitialized -Wreorder -Wundef -Wshadow -Wwrite-strings -Wsign-compare -Wmissing-declarations -D DEBUG -g
debug: $(EXEC)

purge: clean all

setup:
	mkdir -p obj
	
TM: obj/fonction.o obj/m_fonc.o obj/m_trans.o obj/machine.o obj/transition.o obj/UTM.o obj/Parser.o obj/main.o 
	$(CC) -o $@ $^ $(LDFLAGS)
	
obj/fonction.o: src/fonction.cpp
	$(CC) -o $@ -c $< $(C11) $(CFLAGS)

obj/m_fonc.o: src/m_fonc.cpp
	$(CC) -o $@ -c $< $(C11) $(CFLAGS)
	
obj/m_trans.o: src/m_trans.cpp
	$(CC) -o $@ -c $< $(C11) $(NAZIBASE)
	
obj/machine.o: src/machine.cpp
	$(CC) -o $@ -c $< $(C11) $(CFLAGS)
	
obj/transition.o: src/transition.cpp
	$(CC) -o $@ -c $< $(C11) $(CFLAGS)
	
obj/UTM.o: src/UTM.cpp
	$(CC) -o $@ -c $< $(C11) $(CFLAGS)
	
obj/Parser.o: src/Parser.cpp
	$(CC) -o $@ -c $< $(C11) $(CFLAGS)
	
obj/main.o: main.cpp
	$(CC) -o $@ -c $< $(C11) $(CFLAGS)


	
clean:
	rm -f obj/*.o
